all: gnome-disk-health-service gnome-disk-health gnome-disk-health.ui

gnome-disk-health-service: gnome-disk-health-service.vala
	valac --save-temps -g -o $@ --vapidir=. --pkg=atasmart --pkg=hal --pkg=dbus-glib-1 --Xcc="$(shell pkg-config --cflags --libs libatasmart)" $^

gnome-disk-health: gnome-disk-health.vala
	valac --save-temps -g -o $@ --pkg=gtk+-2.0 --pkg=dbus-glib-1 $^

gnome-disk-health.ui: gnome-disk-health.glade
	gtk-builder-convert $< $@

clean:
	rm -f *.o gnome-disk-health-service gnome-disk-health-service.c gnome-disk-health-service.h gnome-disk-health gnome-disk-health.c gnome-disk-health.h gnome-disk-health.ui
